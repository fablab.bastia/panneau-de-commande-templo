## Panneau de commande pour cafetière TEMPLO
Ce projet document la conception et fabrication d'un panneau de commande pour un modèle de cafetière professionnelle TEMPLO (modèle exact inconnu, il n'y a aucun marquage sur la machine et aucune étiquette visible)

C'est une vieille machine, avec toutes les pièces d'origine, et un des panneaux de commandes à fini par cuire et se casser avec le temps (chaleur, vapeur... sans oublier le tact et la délicatesse des opérateurs :D )

Evidemment, aucune infos utiles  sur le net. Donc après quelques prise de mesures et quelques heures sur FreeCAD, le modèle 3D était terminé

La pièce à été imprimée avec des réglages classiques, mais avec du PLA car c'est le seul plastique à disposition à ce moment là. Il est évident que ça ne tiendra pas très longtemps, et qu'il vaudra mieux le réimprimer avec du PETG ou de l'ASA




